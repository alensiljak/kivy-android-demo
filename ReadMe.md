# kivy-android-demo

This project is a demonstration of a Python Kivy web application created in Flask web framework, with a web client UI.

## Dev Setup

In general, you need:

- Android SDK
- Android NDK

On openSuse, you need to install

- python3-devel package, which contains python C headers
- Mesa-devel, for gl.h.

Required Python packages are in requirements.txt. Install them with pip.

Follow the instructions [here](https://python-for-android.readthedocs.io/en/latest/quickstart/#installation) for installation of Python for Android.

## Build a WebView app

`p4a apk --requirements=python3crystax,flask --private $HOME/src/kivy-android-demo/src --arch=armeabi-v7a --package=org.example.myapp --name "My WebView Application" --version 0.1 --bootstrap=webview --port=5000 --sdk_dir $HOME/lib/android-sdk --ndk_dir $HOME/lib/android-sdk/ndk-bundle --ndk-version=r17b`

## Resources

- https://blog.kivy.org/2016/01/python-for-android-now-supports-python-3%C2%A0apks/